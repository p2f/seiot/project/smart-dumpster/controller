/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#include "ServoMotorImpl.h"

ServoMotorImpl::ServoMotorImpl(unsigned pin) {
	this->pin = pin;
	this->angle = 0;
}

unsigned
ServoMotorImpl::getPosition() {
	return this->angle;
}

void
ServoMotorImpl::moveLeft(unsigned angle) {
	int targetAngle = this->getPosition();
	targetAngle -= angle;
	if (targetAngle < 0) {
		targetAngle = 0;
	}
	this->setPosition(targetAngle);
}

void
ServoMotorImpl::moveRight(unsigned angle) {
	unsigned targetAngle = this->getPosition();
	targetAngle += angle;
	if (targetAngle > 180) {
		targetAngle = 180;
	}
	this->setPosition(targetAngle);
}

void
ServoMotorImpl::setPosition(unsigned angle) {
	motor.attach(this->pin);
        // Module 1.3 theory from slide 90
	motor.write(750 + this->angle * angleCoefficient);
    this->angle = angle;
}
