/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#include "Scheduler.h"

void
Scheduler::init(int basePeriod) {
	this->basePeriod = basePeriod;
	timer.setupPeriod(basePeriod);
	taskCount = 0;
}

bool
Scheduler::addTask(Task* task) {
	if (taskCount < MAX_TASKS - 1) {
		taskList[taskCount] = task;
		taskCount++;
		return true;
	} else {
		return false;
	}
}

void
Scheduler::schedule() {
	timer.waitForNextTick();
	for (int i = 0; i < taskCount; i++) {
		if (taskList[i]->updateAndCheckTime(basePeriod)) {
			taskList[i]->tick();
		}
	}
}
