/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#include "SmartDumpsterGlobal.h"

const String SmartDumpsterGlobal::requestSymbol = "request";
const String SmartDumpsterGlobal::openSymbol = "open";
const String SmartDumpsterGlobal::eventSymbol = "event";
const String SmartDumpsterGlobal::openedSymbol = "opened";
const String SmartDumpsterGlobal::extendTimeSymbol = "extend_time";
const String SmartDumpsterGlobal::amountSymbol = "amount";
const String SmartDumpsterGlobal::closingSymbol = "closing";
const String SmartDumpsterGlobal::closedSymbol = "closed";
const String SmartDumpsterGlobal::trashTypeSymbol = "trashtype";
const String SmartDumpsterGlobal::deviceName = "group7IOT";
StateType SmartDumpsterGlobal::state = CLOSED;
const unsigned long SmartDumpsterGlobal::tDeliver = 60;
unsigned long SmartDumpsterGlobal::time;
unsigned long SmartDumpsterGlobal::actualTime;
const unsigned long SmartDumpsterGlobal::maxTime = 300;
TrashType SmartDumpsterGlobal::trash;
unsigned long SmartDumpsterGlobal::actualEchoTime;
const unsigned long SmartDumpsterGlobal::echoTime = 10;
const TrashType SmartDumpsterGlobal::maxTrash = TrashType::PLASTIC;
const char SmartDumpsterGlobal::endChar = '\n';