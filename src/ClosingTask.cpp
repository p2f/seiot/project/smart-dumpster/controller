/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#include "ClosingTask.h"
#include "Arduino.h"
#include "ServoMotorImpl.h"
#include "LedImpl.h"
#include "SmartDumpsterGlobal.h"

ClosingTask::ClosingTask(ServoMotor* motor, Led** leds, JsonMsgService *jsonMsgService) {
	this->motor = motor;
    this->leds = leds;
    this->jsonMsgService = jsonMsgService;
}

void
ClosingTask::init(int period) {
	Task::init(period);
}

void
ClosingTask::tick() {
    if (SmartDumpsterGlobal::state == CLOSING) {
        Serial.println(F("CLOSING TASK"));
        if (this->motor->getPosition() != DUMPSTER_CLOSED) {
            Serial.println(F("CLOSING TASK: I'm closing..."));
            this->motor->setPosition(DUMPSTER_CLOSED);
            this->leds[SmartDumpsterGlobal::trash]->switchOff();
        } else {
            Serial.println(F("CLOSING TASK: Closed"));
            this->jsonMsgService->sendEvent(SmartDumpsterGlobal::closedSymbol);
            SmartDumpsterGlobal::state = CLOSED;
            Serial.println(F("CLOSING TASK: Sent closed message, change state to CLOSED"));
        }
	}
}