/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#include "LedImpl.h"

LedImpl::LedImpl(unsigned pin) {
	this->pin = pin;
	digitalWrite(pin, LOW);
	isOn = false;
}

void
LedImpl::switchOn() {
	digitalWrite(pin, HIGH);
	isOn = true;
}

void
LedImpl::switchOff() {
	digitalWrite(pin, LOW);
	isOn = false;
}

bool
LedImpl::isTurnedOn() {
	return isOn;
}
