/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#include "OpeningTask.h"
#include "Arduino.h"
#include "ServoMotorImpl.h"
#include "LedImpl.h"
#include "SmartDumpsterGlobal.h"

OpeningTask::OpeningTask(ServoMotor* motor, Led** leds, JsonMsgService *jsonMsgService) {
	this->motor = motor;
    this->leds = leds;
    this->jsonMsgService = jsonMsgService;
}

void
OpeningTask::init(int period) {
	Task::init(period);
	this->motor->setPosition(DUMPSTER_CLOSED);
    this->isWaitingForEcho = false;
}

void
OpeningTask::tick() {
    if (SmartDumpsterGlobal::state == OPENING) {
        Serial.println(F("OPENING TASK"));
        if(this->isWaitingForEcho) {
            if((millis() - SmartDumpsterGlobal::actualEchoTime)/1000 < SmartDumpsterGlobal::echoTime) {
                if (this->jsonMsgService->isJsonAvailable()) {
                    Serial.println(F("OPENING TASK: Echo received"));
                    if (this->jsonMsgService->getEvent() == SmartDumpsterGlobal::openedSymbol) {
                        Serial.println(F("OPENING TASK: Correct echo, change state to OPENED"));
                        SmartDumpsterGlobal::actualTime = millis();
                        this->isWaitingForEcho = false;
                        SmartDumpsterGlobal::state = OPENED;
                    } else {
                        Serial.println(F("OPENING TASK: Wrong echo"));
                    }
                }
            } else {
                this->isWaitingForEcho = false;
                SmartDumpsterGlobal::state = CLOSING;
            }
        } else {
            if (this->motor->getPosition() != DUMPSTER_OPENED) {
                Serial.println(F("OPENING TASK: I'm opening..."));
                this->leds[SmartDumpsterGlobal::trash]->switchOn();
                this->motor->setPosition(DUMPSTER_OPENED);
            } else {
                Serial.println(F("OPENING TASK: Opened"));
                this->jsonMsgService->sendEvent(SmartDumpsterGlobal::openedSymbol);
                this->isWaitingForEcho = true;
                SmartDumpsterGlobal::actualEchoTime = millis();
                Serial.println(F("OPENING TASK: Sent opened message, waiting for echo..."));
            }
        }
	}
}