/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#include "OpenedTask.h"
#include "Arduino.h"
#include "SmartDumpsterGlobal.h"

OpenedTask::OpenedTask(JsonMsgService *jsonMsgService) {
    this->jsonMsgService = jsonMsgService;
}

void
OpenedTask::init(int period) {
	Task::init(period);
}

void
OpenedTask::tick() {
    if (SmartDumpsterGlobal::state == OPENED) {
        Serial.println(F("OPENED TASK"));
        if ((millis() - SmartDumpsterGlobal::actualTime)/1000 < SmartDumpsterGlobal::time) {
            if (this->jsonMsgService->isJsonAvailable()) {
                Serial.println(F("OPENED TASK: Request received"));
                if (this->jsonMsgService->getRequest() == SmartDumpsterGlobal::extendTimeSymbol) {
                    unsigned moreTime = this->jsonMsgService->getAmount();
                    if (SmartDumpsterGlobal::time + moreTime <= SmartDumpsterGlobal::maxTime) {
                        Serial.println(F("OPENED TASK: Time updated"));
                        SmartDumpsterGlobal::time += moreTime;
                        this->jsonMsgService->sendEcho();
                    } else {
                        Serial.println(F("OPENED TASK: Time required exceeds limit"));
                        if (SmartDumpsterGlobal::time <= SmartDumpsterGlobal::maxTime) {
                            Serial.println(F("OPENED TASK: Adding remaining time"));
                            this->jsonMsgService->sendExtend(SmartDumpsterGlobal::maxTime - SmartDumpsterGlobal::time);
                            SmartDumpsterGlobal::time = SmartDumpsterGlobal::maxTime;
                        }
                    }
                } else {
                    Serial.println(F("OPENED TASK: Wrong request"));
                }
            }
        } else {
            Serial.println(F("OPENED TASK: Time expired, change state to CLOSING"));
            this->jsonMsgService->sendEvent(SmartDumpsterGlobal::closingSymbol);
            SmartDumpsterGlobal::state = CLOSING;
        }
	}
}