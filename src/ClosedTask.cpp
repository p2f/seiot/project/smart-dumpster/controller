/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#include "ClosedTask.h"
#include "SmartDumpsterGlobal.h"

ClosedTask::ClosedTask(JsonMsgService *jsonMsgService) {
	this->jsonMsgService = jsonMsgService;
}

void
ClosedTask::init(int period) {
	Task::init(period);
}

void
ClosedTask::tick() {
	if (SmartDumpsterGlobal::state == CLOSED) {
        Serial.println(F("CLOSED TASK"));
        if (this->jsonMsgService->isJsonAvailable()) {
            Serial.println(F("CLOSED TASK: Request received"));
            if (this->jsonMsgService->getRequest() == SmartDumpsterGlobal::openSymbol) {
                if (this->jsonMsgService->getTrashType() != TrashType::NOTHING) {
                    Serial.println(F("CLOSED TASK: Correct request, send echo and change state to OPENING"));
                    SmartDumpsterGlobal::state = OPENING;
                    SmartDumpsterGlobal::time = SmartDumpsterGlobal::tDeliver;
                    SmartDumpsterGlobal::trash = this->jsonMsgService->getTrashType();
                    this->jsonMsgService->sendEcho();
                } else {
                    Serial.println(F("CLOSED TASK: Wrong trash type"));
                }
            } else {
                Serial.println(F("CLOSED TASK: Wrong request"));
            }
        }
	}
}
