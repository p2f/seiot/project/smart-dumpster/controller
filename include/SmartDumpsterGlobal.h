/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef SMARTDUMPSTERGLOBAL_H
#define SMARTDUMPSTERGLOBAL_H

#define DUMPSTER_OPENED 0
#define DUMPSTER_CLOSED 180

#include "Arduino.h"
#include "StateType.h"
#include "TrashType.h"

class SmartDumpsterGlobal {
  public:
    static const String requestSymbol;
    static const String openSymbol;
    static const String eventSymbol;
    static const String openedSymbol;
    static const String extendTimeSymbol;
    static const String amountSymbol;
    static const String closingSymbol;
    static const String closedSymbol;
    static const String trashTypeSymbol;
    static const String deviceName;
    static StateType state;
    static const unsigned long tDeliver;
    static unsigned long time;
    static unsigned long actualTime;
    static const unsigned long maxTime;
    static TrashType trash;
    static unsigned long actualEchoTime;
    static const unsigned long echoTime;
    static const TrashType maxTrash;
    static const char endChar;
};

#endif