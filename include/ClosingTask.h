/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef CLOSINGTASK_H
#define CLOSINGTASK_H

#include "ServoMotor.h"
#include "Task.h"
#include "Led.h"
#include "JsonMsgService.h"

/**
 * A task dedicated to the Smart Dumpster closing.
 */
class ClosingTask : public Task {
	ServoMotor* motor;
    Led** leds;
    JsonMsgService* jsonMsgService;

  public:
	/**
	 * Instanciates the task dedicated to the Smart Dumpster opening.
	 * @param motor The ServoMotor object.
	 * @see ServoMotor
     * @param leds The array of leds representing trash types.
     * @see LedImpl
     * @param jsonMsgService The json message service.
     * @see JsonMsgService
	 */
	explicit ClosingTask(ServoMotor* motor, Led** leds, JsonMsgService* jsonMsgService);
	void init(int period);
	void tick() override;
};

#endif