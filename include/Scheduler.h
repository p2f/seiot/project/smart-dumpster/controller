/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "Task.h"
#include "Timer.h"
#define MAX_TASKS 10

/**
 * A task scheduler.
 */
class Scheduler {
	int basePeriod;
	int taskCount;
	Task* taskList[MAX_TASKS];
	Timer timer;

  public:
	/**
	 * Initialize the scheduler.
	 * @param basePeriod the scheduler period.
	 */
	void init(int basePeriod);

	/**
	 * Add a task to be executed by the scheduler.
	 * @param task the task to be executed by the scheduler.
	 * @return if the task has been added to the scheduler.
	 */
	virtual bool addTask(Task* task);

	/**
	 * Execute all the added tasks once.
	 */
	virtual void schedule();
};
#endif
