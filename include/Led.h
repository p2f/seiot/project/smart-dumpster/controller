/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef LED_H
#define LED_H

#include "Light.h"

/**
 * Implements a Led as a Light source.
 */
class Led : public Light {
  public:
	void switchOn() override;
	void switchOff() override;
	bool isTurnedOn() override;
};

#endif
