/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef TRASHTYPE_H
#define TRASHTYPE_H

/**
 * Represents the Smart Dumpster trash types.
 */
enum TrashType {
	GLASS = 0,
	PAPER,
	PLASTIC,
    NOTHING
};

#endif
