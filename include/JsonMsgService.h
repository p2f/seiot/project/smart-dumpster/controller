/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef JSONMSGSERVICE_H
#define JSONMSGSERVICE_H

#include "Arduino.h"
#include "ArduinoJson.h"
#include "BluetoothMsgService.h"
#include "TrashType.h"

/**
 * A controller for json handling.
 */
class JsonMsgService {
  public:
    /**
	 * Instanciates the json controller.
	 * @param jsonMaxElems ththe max number of elements to be handled for each request
	*/
    JsonMsgService(int jsonMaxElems);

    /**
	 * Event getter.
     * @return event.
	 */
	String getEvent();

    /**
	 * TrashType getter.
     * @return trashType.
	 */
	TrashType getTrashType();

    /**
	 * Request getter.
     * @return request.
	 */
	String getRequest();

    /**
	 * Amount getter.
     * @return amount.
	 */
	unsigned getAmount();

	/**
	 * Check if a message is available.
	 * @return if a message is available.
	 */
	bool isJsonAvailable();

    /**
	 * Send an event as json.
	 * @param event the event to be sent as json.
	 */
	void sendEvent(const String& event);

    /**
	 * Send an extend as json.
	 * @param amount the amount to be sent as json.
	 */
    void sendExtend(const unsigned amount);

    /**
	 * Send the last received message back.
	 */
	void sendEcho();
    
  private:
    int capacity;
    String event;
    TrashType trashType;
    String request;
    unsigned amount;
    Msg* currentMsg;
};

#endif
