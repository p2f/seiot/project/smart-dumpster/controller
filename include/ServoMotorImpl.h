/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef SERVOMOTORIMPL_H
#define SERVOMOTORIMPL_H

#include "ServoMotor.h"
#include "ServoTimer2.h"
#include <Arduino.h>

/**
 * A servo motor connected to a pin.
 */
class ServoMotorImpl : public ServoMotor {
  public:
	explicit ServoMotorImpl(unsigned pin);

	unsigned getPosition() override;
	void setPosition(unsigned angle) override;
	void moveLeft(unsigned angle) override;
	void moveRight(unsigned angle) override;

  private:
	// 750 -> 0, 2250 -> 180
	// 750 + angle * (2250 - 750) / 180
	const float angleCoefficient = (2250.0 - 750.0) / 180;
	unsigned pin;
	ServoTimer2 motor;
	unsigned angle;
};

#endif
