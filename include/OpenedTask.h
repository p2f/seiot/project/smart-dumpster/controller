/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef OPENEDTASK_H
#define OPENEDTASK_H

#include "Task.h"
#include "JsonMsgService.h"

/**
 * A task dedicated to the Smart Dumpster opened state.
 */
class OpenedTask : public Task {
    JsonMsgService* jsonMsgService;

  public:
	/**
	 * Instanciates the task dedicated to the Smart Dumpster opened state.
	 * @param jsonMsgService The json message service.
     * @see JsonMsgService
	 */
	explicit OpenedTask(JsonMsgService* jsonMsgService);
	void init(int period);
	void tick() override;
};

#endif