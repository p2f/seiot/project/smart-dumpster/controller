/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef STATETYPE_H
#define STATETYPE_H

/**
 * Represents the Smart Dumpster state.
 */
enum StateType {
	CLOSED,
	OPENING,
    OPENED,
	CLOSING
};

#endif
