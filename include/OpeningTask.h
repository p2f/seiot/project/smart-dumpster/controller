/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef OPENINGTASK_H
#define OPENINGTASK_H

#include "ServoMotor.h"
#include "Task.h"
#include "Led.h"
#include "JsonMsgService.h"

/**
 * A task dedicated to the Smart Dumpster opening.
 */
class OpeningTask : public Task {
	ServoMotor* motor;
    Led** leds;
    JsonMsgService* jsonMsgService;
    bool isWaitingForEcho;

  public:
	/**
	 * Instanciates the task dedicated to the Smart Dumpster opening.
	 * @param motor The ServoMotor object.
	 * @see ServoMotor
     * @param leds The array of leds representing trash types.
     * @see LedImpl
     * @param jjsonMsgService The json message service.
     * @see JsonMsgService
	 */
	explicit OpeningTask(ServoMotor* motor, Led** leds, JsonMsgService* jsonMsgService);
	void init(int period);
	void tick() override;
};

#endif