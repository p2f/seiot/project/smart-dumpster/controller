/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef SERVOMOTOR_H
#define SERVOMOTOR_H

/**
 * A generic servo motor.
 */
class ServoMotor {
  public:
	/**
	 * Get the current motor position.
	 * @return the current motor position.
	 */
	virtual unsigned getPosition() = 0;

	/**
	 * Control the motor to position to a specific angle.
	 * @param angle the angle to be reached by the motor.
	 */
	virtual void setPosition(unsigned angle) = 0;

	/**
	 * Move the motor in the left direction of a given angle.
	 * @param angle the angle distance.
	 */
	virtual void moveLeft(unsigned angle) = 0;

	/**
	 * Move the motor in the right direction of a given angle.
	 * @param angle the angle distance.
	 */
	virtual void moveRight(unsigned angle) = 0;
};

#endif
