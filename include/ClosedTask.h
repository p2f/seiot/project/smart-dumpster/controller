/*
 * IoT 2019/2020
 * Assignment #3: Smart Dumpster
 * Authors:
 *  - Margotta Fabrizio
 *  - Mazzini Pietro
 *  - Righetti Franco
 */

#ifndef CLOSEDTASK_H
#define CLOSEDTASK_H

#include "JsonMsgService.h"
#include "Task.h"

/**
 * A task dedicated to the bluetooth listening activity.
 */
class ClosedTask : public Task {
    JsonMsgService* jsonMsgService;

  public:
    /**
	 * Instanciates the task dedicated to the bluetooth listening.
     * @param jsonMsgService The JsonMsgService object.
	 * @see JsonMsgService
	 */
	explicit ClosedTask(JsonMsgService *jsonMsgService);
	void init(int period);
	void tick() override;
};

#endif
